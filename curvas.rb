def entities
    Sketchup.active_model.entities
end

def operation description="operation"
    model = entities.model
    model.start_operation description
    yield
    model.commit_operation
end

def face *args
    entities.add_face args
end

def color *args
    if args.length == 1
        Sketchup::Color.new args[0]
    else
        Sketchup::Color.new args
    end
end

def rectangle width:, length:, position: [0,0,0], rotation: [0,0,0]
    half_width = width*0.5
    half_length = length*0.5

    transform =
        lambda {|point| point.rotate_on(x: rotation.x, y: rotation.y, z: rotation.z).plus position}

    a = transform.call [-half_width, -half_length, 0]
    b = transform.call [ half_width, -half_length, 0]
    c = transform.call [ half_width,  half_length, 0]
    d = transform.call [-half_width,  half_length, 0]

    face a,b,c,d
end

def disk radius:, position: [0,0,0], rotation: [0,0,0]
    normal = [0,0,1].rotate_on x: rotation.x, y: rotation.y, z: rotation.z
    face (entities.add_circle position, normal, radius, 48)
end

def polygon radius:, sides:, position: [0,0,0], rotation: [0,0,0]
    normal = [0,0,1].rotate_on x: rotation.x, y: rotation.y, z: rotation.z
    face (entities.add_ngon position, normal, radius, sides)
end

def box width:, length:, height:, position: [0,0,0], rotation: [0,0,0]
    z = -0.5*height
    # Sketchup inverts normals if the face is on the xy plane
    height = -height if z + position.z == 0

    shifted = position.plus [0, 0, z]
    rectangle(width: width, length: length, position: shifted, rotation: rotation).
        pushpull(height)
end

def cylinder radius:, height:, position: [0,0,0], rotation: [0,0,0]
    z = -0.5*height
    # Sketchup inverts normals if the face is on the xy plane
    height = -height if z + position.z == 0

    shifted = position.plus [0, 0, z]
    disk(radius: radius, position: shifted, rotation: rotation).
        pushpull(height)
end

def sphere radius:,  position: [0,0,0]
    normal = [0,0,1].rotate_on x: 90.degrees
    circle = entities.add_circle (position.plus [0, 2*radius, 0]), normal, radius
    disk(radius: radius, position: position).followme(circle)
    circle.each {|edge| edge.erase!}
    nil
end

def group *objects, position: [0,0,0], rotation: [0,0,0], rotation_center: nil, scale: 1, args: [], &block
    rotation_center = rotation_center || position

    translation = Geom::Transformation.translation position
    along_x = Geom::Transformation.rotation rotation_center, [1, 0, 0], rotation.x
    along_y = Geom::Transformation.rotation rotation_center, [0, 1, 0], rotation.y
    along_z = Geom::Transformation.rotation rotation_center, [0, 0, 1], rotation.z
    scaling = Geom::Transformation.scaling position, scale

    new_group =
        if objects.length > 0
            entities.add_group(objects)
        else
            entities.add_group
        end

    new_group.
        run(args, &block).
        transform!(along_z * along_y * along_x * translation * scaling)
end

def curve length:, height:, interval: [-1,1], position: [0,0,0], rotation: [0,0,0], edges: 40, closed: false
    points = []

    (-1..1).step (1.0/edges) do |n|
        k = 0.5*(interval[1] - interval[0])*n + 0.5*(interval[1] + interval[0])
        point =
            [0.5*n*length, 0, height*(yield k)].
            plus(position).
            rotate_on x: rotation.x, y: rotation.y, z: rotation.z

        points.push point
    end

    points.push points[0] if closed

    entities.add_curve points
end

def parametric_curve size:, interval: [0,1], position: [0,0,0], rotation: [0,0,0], edges: 40
    points = []

    (0..1).step (1.0/edges) do |n|
        k = n*interval[1] - (1-n)*interval[0]
        point_2d = yield k
        point =
            [size*point_2d.x, size*point_2d.y, size*(point_2d.z || 0)].
            plus(position).
            rotate_on x: rotation.x, y: rotation.y, z: rotation.z

        points.push point
    end

    entities.add_curve points
end

def rotated_curve length:, radius:, turns: 1, interval: [-1,1], position: [0,0,0], rotation: [0,0,0], edges: 40
    points = []

    (-1..1).step (1.0/edges) do |n|
        angle = turns*Math::PI*(n+1)
        k = 0.5*(interval[1] - interval[0])*n + 0.5*(interval[1] + interval[0])
        r = radius*(yield k)

        point = 
            [0.5*n*length, r*Math::cos(angle), r*Math::sin(angle)].
            plus(position).
            rotate_on x: rotation.x, y: rotation.y, z: rotation.z

        points.push point
    end

    entities.add_curve points
end

def surface closed: false, &block
    Surface.new.
        run(&block).
        generate parent_entities: entities, closed: closed
end

def sculpt face:, length:, turns: 0, interval: [-1,1], edges: 40
    vertices = face.vertices.map { |v| v.position }
    mesh = Geom::PolygonMesh.new
    number_of_vertices = vertices.size
    lower_points = vertices

    (0..1).step (1.0/edges) do |n|
        k = (interval[1] - interval[0])*n + interval[0]
        upper_points = face.scale_points vertices, (yield k)
        upper_points = face.translate_points upper_points, n*length
        upper_points = face.rotate_points upper_points, (turns*2*Math::PI*n) if turns != 0

        if n == 0
            mesh.add_polygon upper_points
        else
            number_of_vertices.times do |i|
                lower = Looper.new lower_points
                upper = Looper.new upper_points
                mesh.add_polygon [lower[i], lower[i+1], upper[i]]
                mesh.add_polygon [lower[i+1], upper[i], upper[i+1]]
            end
        end

        lower_points = upper_points
    end

    mesh.add_polygon lower_points
    entities.add_faces_from_mesh mesh
end

def grid *objects, amount:, gaps:,  position: [0,0,0], rotation: [0,0,0], &block
    shift = amount.map.with_index {|n, i| -0.5*(n-1)*gaps[i]}

    group position: position, rotation: rotation do
        new_group = entities.add_group(objects)

        amount.x.times do |i|
            amount.y.times do |j|
                amount.z.times do |k|
                    position = [i*gaps.x + shift.x, j*gaps.y + shift.y, k*gaps.z + shift.z]
                    if objects.length > 0
                        group(new_group.copy.make_unique, position: position, args: [i,j,k], &block)
                    else
                        group(position: position, args: [i,j,k], &block)
                    end
                end
            end
        end

        new_group.erase! if new_group != nil
    end
end

def ring *objects, amount:, radius:, position: [0,0,0], rotation: [0,0,0], &block
    angle = 2*Math::PI/amount

    group position: position, rotation: rotation do
        new_group = entities.add_group(objects)

        amount.times do |n|
            if objects.length > 0
                group(new_group.copy.make_unique, position: [radius, 0, 0], rotation: [0, 0, n*angle], rotation_center: ORIGIN, args: n, &block)
            else
                group(position: [radius, 0, 0], rotation: [0, 0, n*angle], rotation_center: ORIGIN, args: n, &block)
            end
        end

        new_group.erase! if new_group != nil
    end
end

def path *objects, curve:, amount:, uniform: false, position: [0,0,0], rotation: [0,0,0], &block
    if uniform
        # Use uniform spacement along the curve
        segment_length = curve[0].curve.length/(amount - 1)

        total_length = 0
        counter = 0

        group(position: position, rotation: rotation) do
            new_group = entities.add_group(objects)

            curve.each do |edge|
                if counter*segment_length <= total_length + edge.length
                    weight = (counter*segment_length - total_length)/(edge.length)
                    start_point = edge.start.position
                    end_point = edge.end.position

                    position =
                        Geom::Point3d.linear_combination (1-weight), start_point, weight, end_point

                    if objects.length > 0
                        group(new_group.copy.make_unique, position: position, args: counter, &block)
                    else
                        group(position: position, args: counter, &block)
                    end

                    new_group.erase! if new_group != nil
                    counter += 1
                    break if counter >= amount
                end

                total_length += edge.length
            end
        end
    else
        # Use uniform spacement along the x axis
        points = curve.size
        step = (points - 1.0)/(amount - 1)

        group(position: position, rotation: rotation) do
            counter = 0

            (0..points).step step do |k|
                edge = curve[k.floor]
                weight = k - k.floor
                start_point = edge.start.position
                end_point = edge.end.position

                position =
                    Geom::Point3d.linear_combination (1-weight), start_point, weight, end_point

                group position: position, args: counter, &block
                counter += 1
            end
        end
    end
end

def mirror rotation: [0,0,0], &block
    along_x = Geom::Transformation.rotation ORIGIN, [1, 0, 0], rotation.x
    along_y = Geom::Transformation.rotation ORIGIN, [0, 1, 0], rotation.y
    along_z = Geom::Transformation.rotation ORIGIN, [0, 0, 1], rotation.z

    normal = Geom::Vector3d.new([1,0,0]).transform along_x*along_y*along_z

    a = normal.x
    b = normal.y
    c = normal.z

    matrix =
        [ 1 - 2*a*a, -2*a*b,     -2*a*c,     0,
         -2*a*b,      1 - 2*b*b, -2*b*c,     0,
         -2*a*c,     -2*b*c,      1 - 2*c*c, 0,
          0,          0,          0,         1]

    reflection = Geom::Transformation.new matrix
    original = group(&block)
    original.copy.transform! reflection
end 

def union &block
    group(&block).union!
end

def outer_shell &block
    group(&block).outer_shell!
end

def intersection &block
    group(&block).intersection!
end

class Sketchup::Color
    def *(weight)
        self.blend (color 0,0,0), weight
    end

    def +(other)
        self.blend other, 0.5
    end
end

class Sketchup::Face
    def scale_points points, factor
        points.map do |point|
            scaling = Geom::Transformation.scaling [0,0,0], (1- factor)
            offset = (self.bounds.center - point).transform scaling
            point.offset offset
        end
    end

    def rotate_points points, angle
        rotation = Geom::Transformation.rotation self.bounds.center, self.normal, angle
        points.map { |point| point.transform rotation }
    end

    def translate_points points, shift
        translation = Geom::Transformation.translation (self.normal.to_a.times shift)
        points.map { |point| point.transform translation }
    end
end

class Surface
    def initialize
        @curves = []
    end

    def add_curve curve
        @curves.push curve
    end

    # When an `entities` message arrives inside a Surface block, return self in
    # order to use its own `add_curve` method
    def entities
        self
    end

    def run &block
        instance_eval(&block)
        self
    end

    def generate parent_entities:, closed:
        number_of_points = @curves.first.size

        if @curves.any? { |curve| curve.size != number_of_points }
            raise "The provided curves have different number of points"
        end

        number_of_curves = closed ? @curves.size : @curves.size - 1
        mesh = Geom::PolygonMesh.new
        curves = Looper.new @curves

        number_of_curves.times do |i|
            (number_of_points - 1).times do |j|
                first_curve = curves[i]
                second_curve = curves[i+1]

                lower_triangle =
                    [ first_curve[j],
                      second_curve[j],
                      first_curve[j+1] ]

                upper_triangle =
                    [ second_curve[j],
                      second_curve[j+1],
                      first_curve[j+1] ]

                mesh.add_polygon lower_triangle
                mesh.add_polygon upper_triangle
            end
        end

        parent_entities.add_faces_from_mesh mesh
    end
end

class Looper
    def initialize(array)
        @array = array
    end

    def [](i)
        @array[i % @array.size]
    end
end

class Array
    def plus other
        if self.size != other.size
            raise "Trying to add an array of size #{self.size} to and array of size #{other.size}"
        end

        self.map.with_index {|e, i| e + other[i]}
    end

    def times factor
        self.map {|e| e*factor}
    end

    def transform transformation
        if self.size != 3
            raise "An array must be tridimensional to apply a transformation"
        end

        point = Geom::Point3d.new(self).transform(transformation)
        [point.x, point.y, point.z]
    end

    def rotate_on x: 0, y: 0, z: 0, origin: [0, 0, 0]
        along_x = Geom::Transformation.rotation origin, [1, 0, 0], x
        along_y = Geom::Transformation.rotation origin, [0, 1, 0], y
        along_z = Geom::Transformation.rotation origin, [0, 0, 1], z

        self.transform (along_z * along_y * along_x)
    end
end

class Sketchup::Group
    def run *args, &block
        instance_exec(*args, &block)
        self
    end

    def translate! shift
        self.transform! Geom::Transformation.translation shift
    end

    def rotate! angles, origin = self.bounds.center
        along_x = Geom::Transformation.rotation origin, [1, 0, 0], angles.x
        along_y = Geom::Transformation.rotation origin, [0, 1, 0], angles.y
        along_z = Geom::Transformation.rotation origin, [0, 0, 1], angles.z

        self.transform! (along_z * along_y * along_x)
    end

    def scale! factor
        self.transform! Geom::Transformation.scaling self.bounds.center, factor
    end

    def -(other)
        other.flatten!.subtract self.flatten!
    end

    def /(other)
        other.flatten!.split self.flatten!
    end

    def flatten!
        self.entities.grep(Sketchup::Group) do |group|
            if group.entities.grep(Sketchup::Group).length > 0
                group.flatten!
            end

            group.explode
        end

        self
    end

    def union!
        groups = self.entities.grep Sketchup::Group
        united = groups[0].flatten!

        (1..groups.length-1).each do |i|
            united = united.union groups[i].flatten!
        end

        self
    end

    def outer_shell!
        groups = self.entities.grep Sketchup::Group
        shell = groups[0].flatten!

        (1..groups.length-1).each do |i|
            shell = shell.outer_shell groups[i].flatten!
        end

        self
    end


    def intersection!
        groups = self.entities.grep Sketchup::Group
        intersected = groups[0].flatten!

        (1..groups.length-1).each do |i|
            intersected = intersected.intersect groups[i].flatten!
        end

        self
    end
end
