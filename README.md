# Curvas

*Curvas* is a small SketchUp library for declarative parametric and
procedural modelling using [Ruby](https://www.ruby-lang.org). It relies
on some powerful abstractions provided by Ruby to build complex 3D
models by composing simpler ones in a straightforward and succinct
manner.

``` ruby
group position: [1.m, 0, 50.cm], rotation: [0, 30.degrees, 0] do
    surface do
        curve length: 5.m, height: 30.cm do |x| Math::sin(2*Math::PI*x) end
        curve length: 5.m, height: 30.cm, position: [0, 1.m, 0] do |x| Math::sin(4*Math::PI*x) end
        curve length: 5.m, height: 30.cm, position: [0, 2.m, 0] do |x| Math::sin(6*Math::PI*x) end
    end
    
    group position: [30.cm, 0, 0] do
        ring amount: 6 do
            rotated_curve length: 2.m, radius: 1.m, turns: 2 do |x| x end
        end
    end
end

10.times do |n|
    rectangle width: 50.cm, length: 1.m, position: [0, 0, n*20.cm], rotation: [0, 0, n*Math::PI/10]
end
```

## Table of contents

  - [Usage](#usage)
  - [Documentation](#documentation)
      - [Basic geometries](#basic-geometries)
          - [face](#face)
          - [rectangle](#rectangle)
          - [disk](#disk)
          - [polygon](#polygon)
          - [box](#box)
          - [sphere](#sphere)
          - [cylinder](#cylinder)
      - [Mathematical curves](#mathematical-curves)
          - [curve](#curve)
          - [rotated\_curve](#rotated_curve)
          - [parametric\_curve](#parametric_curve)
          - [sculpt](#sculpt)
      - [Object composition](#object-composition)
          - [group](#group)
          - [mirror](#mirror)
          - [grid](#grid)
          - [ring](#ring)
          - [path](#path)
          - [surface](#surface)

## Usage

The easiest way to use this library is installing the
[Console+](https://github.com/Aerilius/sketchup-console-plus) plugin.
After installing it, you can open the file `curvas.rb` in the editor,
press the `Play` button to load the code and then switch to the
*console* tab in the plugin to start writing commands.

## Documentation

### Basic geometries

#### face

The `face` function generates a face (a flat surface) from a list of
vertices:

``` ruby
face [0,0,0], [1,0,0], [0,1,0]
```

The above code generates a triangle with vertices A = (0,0,0), B =
(1,0,0) and C = (0,1,0)

#### rectangle

The `rectangle` function generates a rectangle:

``` ruby
rectangle width: 1.m, length: 50.cm, position: [0, 0, 2.m], rotation: [0, 0, 30.degrees]
```

| Argument              | Default value | Description                                                                                                                                                            |
| --------------------- | ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `width`               | *none*        | Rectangle's width                                                                                                                                                      |
| `length`              | *none*        | Rectangle's length                                                                                                                                                     |
| `position` (optional) | \[0,0,0\]     | Rectangle's center position                                                                                                                                            |
| `rotation` (optional) | \[0,0,0\]     | The argument `rotation` allows rotating the rectangle specifying rotation angles as a triple `[x,y,z]` in [Euler notation](https://en.wikipedia.org/wiki/Euler_angles) |

#### disk

The `disk` function generates a disk:

``` ruby
disk radius: 30.cm
```

| Argument              | Default value | Description            |
| --------------------- | ------------- | ---------------------- |
| `radius`              | *none*        | Disk's radius          |
| `position` (optional) | \[0,0,0\]     | Disk's center position |
| `rotation` (optional) | \[0,0,0\]     | Disk's rotation        |

#### polygon

The `polygon` function generates a polygon:

``` ruby
polygon radius: 1.m, sides: 8
```

| Argument              | Default value | Description               |
| --------------------- | ------------- | ------------------------- |
| `radius`              | *none*        | Polygon's radius          |
| `sides`               | *none*        | Number of sides           |
| `position` (optional) | \[0,0,0\]     | Polygon's center position |
| `rotation` (optional) | \[0,0,0\]     | Polygon's rotation        |

#### box

The `box` function generates a parallelepiped:

``` ruby
box width: 1.m, length: 2.m, height: 3.m
```

| Argument              | Default value | Description           |
| --------------------- | ------------- | --------------------- |
| `width`               | *none*        | Box's width           |
| `length`              | *none*        | Box's length          |
| `height`              | *none*        | Box's height          |
| `position` (optional) | \[0,0,0\]     | Box's center position |
| `rotation` (optional) | \[0,0,0\]     | Box's rotation        |

#### sphere

The `sphere` function generates a sphere:

``` ruby
sphere radius: 1.m
```

| Argument              | Default value | Description              |
| --------------------- | ------------- | ------------------------ |
| `radius`              | *none*        | Sphere's radius          |
| `position` (optional) | \[0,0,0\]     | Sphere's center position |

#### cylinder

The `cylinder` function generates a cylinder:

``` ruby
cylinder radius: 30.cm, height: 3.m
```

| Argument              | Default value | Description                |
| --------------------- | ------------- | -------------------------- |
| `radius`              | *none*        | Cylinder's radius          |
| `height`              | *none*        | Cylinder's height          |
| `position` (optional) | \[0,0,0\]     | Cylinder's center position |
| `rotation` (optional) | \[0,0,0\]     | Cylinder's rotation        |

### Mathematical curves

#### curve

The `curve` function generates a mathematical curve in a plane. The
curve is described as an equation given by the provided code block:

``` ruby
curve length: 5.m, height: 1.m do |x| 4*(x - 0.5)**2 end
```

The above example draws the curve specified by the equation f(x) = 4 (x
- 0.5)², which defines a parabola. The code block argument (called `x`
in the example) lies in the range \[-1,1\] (that is: -1 ≤ x ≤ 1).

| Argument              | Default value | Description                                                                                                                         |
| --------------------- | ------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| `length`              | *none*        | Curve's length                                                                                                                      |
| `height`              | *none*        | Curve's height; in other words, the value returned by the code block will be multiplied by the value given by the `heigh` parameter |
| `interval` (optional) | \[-1,1\]      | The domain of the function, specified as an array with two elements                                                                 |
| `closed`              | false         | Whether it's a closed curve                                                                                                         |
| `position` (optional) | \[0,0,0\]     | Curve's center position                                                                                                             |
| `rotation` (optional) | \[0,0,0\]     | Curve's rotation                                                                                                                    |
| `edges` (optional)    | 40            | The amount of edges the resulting curve will have                                                                                   |

To draw a vertical curve, you just need to rotate it around the *y*
axis:

``` ruby
curve length: 5.m, height: 1.m, rotation: [0, 270.degrees, 0] do |x| x end
```

#### rotated\_curve

The `rotated_curve` function is similar to the previous `curve` function
but it draws the curve rotating it around an axis:

``` ruby
rotated_curve length: 2.m, height: 1.m turns: 2 do |x| 1 - x end
```

As with `curve`, -1 ≤ x ≤ 1.

The above example draws the rotated\_curve specified by the equation
f(x) = 4 (x - 0.5)², which defines a parabola. The code block argument
(called `x` in the example) lies in the range \[-1,1\] (that is: -1 ≤ x
≤ 1).

| Argument              | Default value | Description                                                                                          |
| --------------------- | ------------- | ---------------------------------------------------------------------------------------------------- |
| `length`              | *none*        | Curve's length                                                                                       |
| `radius`              | *none*        | The value given by the `radius` parameter will be multiplied by the value returned by the code block |
| `turns` (optional)    | 1             | Number of times the curve will turn around its axis                                                  |
| `interval` (optional) | \[-1,1\]      | The domain of the function, specified as an array with two elements                                  |
| `position` (optional) | \[0,0,0\]     | Curve's center position                                                                              |
| `rotation` (optional) | \[0,0,0\]     | Curve's rotation                                                                                     |
| `edges` (optional)    | 40            | The amount of edges the resulting curve will have                                                    |

#### parametric\_curve

The `parametric_curve` function draws curves defined by parametric
equations, that is to say, functions that generate two-dimensional or
three-dimensional coordinates, like f(t) = (cos(t), sin(t)) for a circle
or f(t) = (cos(t), sin(t), t) for an spiral:

``` ruby
parametric_curve size: 1.m, interval: [0, 2*Math::PI], edges: 400 do |x| [Math::sin(12*t), Math::cos(11*t)] end
```

Different from the preceding functions, `parametric_curve` uses 0 ≤ x ≤
1 by default.

| Argument              | Default value | Description                                                                   |
| --------------------- | ------------- | ----------------------------------------------------------------------------- |
| `size`                | *none*        | Curve's size, that is the value the returned coordinate will be multiplied by |
| `interval` (optional) | \[0,1\]       | The domain of the function, specified as an array of two elements             |
| `position` (optional) | \[0,0,0\]     | Curve's center position                                                       |
| `rotation` (optional) | \[0,0,0\]     | Curve's rotation                                                              |
| `edges` (optional)    | 40            | The amount of edges the resulting curve will have                             |

#### sculpt

The `sculpt` generates a solid from a face. It's similar to `followme`
except it allows prescribing the solid's shape using a mathematical
curve:

``` ruby
square = rectangle width: 50.cm, length: 50.cm
sculpt face: square, length: 2.m, turns: 0.25 do |x| x**2 + 0.5 end
```

| Argument              | Default value | Description                                                                                          |
| --------------------- | ------------- | ---------------------------------------------------------------------------------------------------- |
| `face`                | *none*        | The face used as a base for the solid                                                                |
| `length`              | *none*        | Solid's length                                                                                       |
| `radius`              | *none*        | The value given by the `radius` parameter will be multiplied by the value returned by the code block |
| `turns` (optional)    | 1             | Number of times the solid will turn around its axis                                                  |
| `interval` (optional) | \[-1,1\]      | The domain of the function, specified as an array of two elements                                    |
| `edges` (optional)    | 40            | The amount of edges used to generate the solid                                                       |

### Object composition

#### group

The `group` function generates a group with any elements defined within
its code block:

``` ruby
group rotation: [0, 60.degrees, 0] do
    face [0, 0, 0], [0, 1.m, 0], [1.m, 0, 0]
    rectangle width: 50.cm, length: 40.cm
end
```

The above example generates a group containing a triangle and a
rectangle.

| Argument                     | Default value | Description                                                                                          |
| ---------------------------- | ------------- | ---------------------------------------------------------------------------------------------------- |
| `position` (optional)        | \[0,0,0\]     | Group's center position                                                                              |
| `rotation` (optional)        | \[0,0,0\]     | Group's rotation                                                                                     |
| `rotation_center` (optional) | \[0,0,0\]     | Center of the rotation specified by the argument `rotation`. The default value is the group's center |

#### mirror

The `mirror` function generates a second group, mirrored respecting the
provided elements, using *yz* as the reflection plane:

``` ruby
mirror do
    box width: 50.cm, length: 40.cm, height: 30.cm, position: [1.m, 0, 0]
    cylinder radius: 30.cm, height: 1.m, position: [40.cm, 0, 0]
end
```

| Argument              | Default value | Description                      |
| --------------------- | ------------- | -------------------------------- |
| `rotation` (optional) | \[0,0,0\]     | Rotation of the reflection plane |

#### grid

The `grid` function generates a three-dimensional grid of objects:

``` ruby
grid amount: [2,3,2], gaps: [1.m, 1.m, 1.m] do
    box width: 50.cm, length: 50.cm, height: 50.cm
    sphere radius: 55.cm
end
```

It's possible to make variations inside the grid by using the arguments
passed to the code block:

``` ruby
grid amount: [2,3,2], gaps: [1.m, 1.m, 1.m] do |column, line, layer|
    box width: (line + 1).cm, length: (column + 1).cm, height: (layer + 1).cm
    sphere radius: (line + 1)*10.cm
end
```

| Argument              | Default value | Description                                                                                                                                   |
| --------------------- | ------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| `amount`              | *none*        | The amount of elements, specified as a tripe \[i,j,k\], being *i* the amount of lines, *j* the amount of columns and *k* the amount of layers |
| `gaps`                | *none*        | The distances between an element an its neighbour, specified as a tripe \[x,y,z\]                                                             |
| `position` (optional) | \[0,0,0\]     | Grid's center position                                                                                                                        |
| `rotation` (optional) | \[0,0,0\]     | Grid's rotation                                                                                                                               |

#### ring

The `ring` function generates a circular ring of objects:

``` ruby
ring amount: 5, radius: 1.m do
    box width: 50.cm, length: 50.cm, height: 50.cm
    sphere radius: 55.cm
end
```

It's possible to make variations inside the ring by using the number of
each group passed as an argument the code block:

``` ruby
ring amount: 5, radius: 1.m do |n|
    sphere radius: (n + 1)*55.cm
end
```

| Argument              | Default value | Description                                |
| --------------------- | ------------- | ------------------------------------------ |
| `amount`              | *none*        | The amount of elements (an integer number) |
| `radius`              | *none*        | Ring's radius                              |
| `position` (optional) | \[0,0,0\]     | Ring's center position                     |
| `rotation` (optional) | \[0,0,0\]     | Ring's rotation                            |

#### path

The `path` function organise objects along a curve:

``` ruby
parabola = curve length: 5.m, height: 1.m do |x| (x - 0.5)**2 end

path curve: parabola, amount: 5 do
    box width: 50.cm, length: 50.cm, height: 50.cm
    sphere radius: 55.cm
end
```

It's possible to make variations along the path by using the number of
each group passed as an argument the code block:

``` ruby
parabola = curve length: 5.m, height: 1.m do |x| (x - 0.5)**2 end

path curve: parabola, amount: 5 do |n|
    sphere radius: (n + 1)*55.cm
end
```

| Argument              | Default value | Description                                                                                                                                                                                                          |
| --------------------- | ------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `curve`               | *none*        | The curve along which the elements will be placed                                                                                                                                                                    |
| `amount`              | *none*        | The amount of elements to be put along the curve (an integer number)                                                                                                                                                 |
| `uniform`             | false         | Whether the elements will be placed at a regular distance between each other. The value `true` makes a regular spacing throughout the curve, while the value `false` makes a regular spacing throughout the *x* axis |
| `position` (optional) | \[0,0,0\]     | Curve's center position                                                                                                                                                                                              |
| `rotation` (optional) | \[0,0,0\]     | Curve's rotation                                                                                                                                                                                                     |

#### surface

The `surface` function generates a surface from a colection of curves:

``` ruby
surface do
    curve length: 5.m, height: 30.cm do |x| Math::sin(2*Math::PI*x)
    curve length: 5.m, height: 30.cm, position: [0, 1.m, 0] do |x| Math::sin(4*Math::PI*x) end
    curve length: 5.m, height: 30.cm, position: [0, 2.m, 0] do |x| Math::sin(6*Math::PI*x) end
end
```

| Argument | Default value | Description                   |
| -------- | ------------- | ----------------------------- |
| `closed` | false         | Whether it's a closed surface |
